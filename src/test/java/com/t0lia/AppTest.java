package com.t0lia;

import org.testng.Assert;
import org.testng.annotations.Test;

public class AppTest {
    @Test
    public void testName() {
        Assert.assertEquals(new App().foo(), "bar");
    }
}